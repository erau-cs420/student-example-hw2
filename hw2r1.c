#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>
#include<errno.h>

void *thread2(void *testnum);

int main()
{
    //identify self
    printf("  main thread here; pid is %d, tid is %lu\n", getpid(), pthread_self());
    
    //new thread
    pthread_t newThread;
    if(pthread_create(&newThread, NULL, &thread2, NULL) == 0)
        printf("  main: succesfully created a new thread with TID of %lu\n", newThread);
    
}


//second thread doesn't run
void *thread2(void *testnum)
{
    printf("\n  second thread here, my tid is %lu but I'm still in process %d\n", pthread_self(), getpid());
    return NULL;
}
