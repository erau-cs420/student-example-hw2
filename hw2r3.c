#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>
#include<errno.h>

void *thread2(void *someChar);

int main()
{
    //identify self
    printf("  main thread here; pid is %d, tid is %lu\n", getpid(), pthread_self());
    
    //new thread and create a simplified Negative Squared Latin Capital Letter 'B' to pass to new thread
    pthread_t newThread;
    char *letter = (char *)'B';
    
    //success
    if(pthread_create(&newThread, NULL, &thread2, letter) == 0)
        printf("  main: succesfully created a new thread with TID of %lu\n", newThread);
    
    
    //run the new thread before termination
    pthread_join(newThread, NULL);
    
    printf("\n  main: second thread has terminated, main will now exit.\n");
    
}



void *thread2(void *someChar)
{
    printf("\n    second thread here, my tid is %lu but I'm still in process %d\n", pthread_self(), getpid());
    printf("    I received '%c' when I was created\n", (char *)someChar);
    return NULL;
}
